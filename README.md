<p align="center">
    <a href="https://www.docker.com/" target="_blank">
        <img src="https://www.docker.com/sites/default/files/mono_vertical_large.png" height="100px">
    </a>
    <h1 align="center">Docker image for testing (CI)</h1>
    <br>
</p>

**Stable** 
[![pipeline status](https://gitlab.com/exileed/php-testing/badges/master/pipeline.svg)](https://gitlab.com/exileed/php-testing/commits/master)


## About

These Docker images are built on top of the official PHP Docker image (alpine), they contain additional PHP extensions required to testing (CI, but no code of the framework itself.
The `Dockerfile`(s) of this repository are designed to build from different PHP-versions by using *build arguments*.

### Available versions for `fever/php-testing`

```
8.0, 7.4
```


### Available extensions

```
amqp
curl
iconv
mbstring
pdo
pdo_mysql
pdo_pgsql
pdo_sqlite
redis
pcntl
tokenizer
xml
gd
bcmath
soap
sockets
intl
imagick (not yet for php8.0)
exif
xdebug
zip
kafka
```

### Available software

```
yarn
npm
```


### Example build

`docker build -t fever/php-testing:7.4 --build-arg PHP_BASE_IMAGE_VERSION=7.4 .`