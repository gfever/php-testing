ARG PHP_BASE_IMAGE_VERSION

FROM php:${PHP_BASE_IMAGE_VERSION}-fpm-alpine

MAINTAINER Dmitriy Kuts <me@exileed.com>

# Install dev dependencies
RUN apk add --no-cache --update --virtual .build-deps \
    $PHPIZE_DEPS \
    make \
    g++ \
    gcc \
    curl-dev \
    imagemagick-dev \
    oniguruma-dev \
    libc-dev \
    libpng-dev \
    icu-dev \
    libtool \
    libxml2-dev \
    postgresql-dev \
    sqlite-dev \
    rabbitmq-c-dev \
    # Install production dependencies
 && apk add --update --no-cache \
    bash \
    curl \
    git \
    imagemagick \
    icu \
    mysql-client \
    openssh-client \
    postgresql-libs \
    rsync \
    zlib-dev \
    libzip-dev \
    rabbitmq-c \
    nodejs \
    yarn \
    librdkafka-dev

RUN pecl install -o -f amqp \
  && pecl install -o -f redis \
  && pecl install -o -f rdkafka

# Install PECL and PEAR extensions
RUN yes | pecl install \
    imagick \
    xdebug
# Install and enable php extensions
RUN docker-php-ext-enable \
    imagick \
    redis \
    xdebug \
    amqp \
    rdkafka \
    && docker-php-ext-configure zip
RUN docker-php-ext-install \
    curl \
    iconv \
    mbstring \
    pdo \
    pdo_mysql \
    pdo_pgsql \
    pdo_sqlite \
    pcntl \
    tokenizer \
    xml \
    gd \
    bcmath \
    sockets \
    intl \
    exif \
    soap \
    zip

RUN echo 'memory_limit = 1G' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini
RUN echo 'xdebug.mode = coverage' >> /usr/local/etc/php/conf.d/xdebug.ini

RUN curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/*
